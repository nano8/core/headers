<?php

namespace laylatichy\nano\core;

use JetBrains\PhpStorm\Pure;

final class Headers {
    public function __construct(
        private array $headers = [],
        private array $base = [],
    ) {
        $this->init();
    }

    #[Pure]
    public function get(string $header): string {
        return $this->headers[$header] ?? '';
    }

    #[Pure]
    public function getAll(): array {
        return $this->headers;
    }

    public function add(string $header, string $value): void {
        $this->headers[$header] = $value;
    }

    public function base(string $header, string $value): void {
        $this->base[$header] = $value;
    }

    public function clean(): void {
        $this->headers = [];
    }

    public function bootstrap(): void {
        $this->init();
    }

    private function init(): void {
        $this->clean();
        $this->add(
            header: 'Server',
            value: 'laylatichy/nano'
        );
        $this->add(
            header: 'Version',
            value: '2.0.*'
        );
        $this->add(
            header: 'Access-Control-Allow-Methods',
            value: 'POST, GET, OPTIONS'
        );
        $this->add(
            header: 'Access-Control-Allow-Headers',
            value: 'content-type'
        );
        foreach ($this->base as $header => $value) {
            $this->add($header, $value);
        }
    }
}
