### nano/core/headers

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-headers?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-headers)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-headers?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-headers)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/headers/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/headers/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/headers/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/headers/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/headers/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/headers/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-headers`
